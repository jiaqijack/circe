[ ![Download](https://api.bintray.com/packages/jiaqijack/Maven/Circe/images/download.svg) ](https://bintray.com/jiaqijack/Maven/Circe/_latestVersion) [ ![Build Status](https://img.shields.io/badge/build-passing-brightgreen.svg) ](https://jiaqijack.bitbucket.io/Circe/site/surefire-report.html) [ ![Code Coverage](https://img.shields.io/badge/coverage-100%25-brightgreen.svg) ](https://jiaqijack.bitbucket.io/Circe/site/jacoco/index.html) [ ![JavaDoc](https://img.shields.io/badge/JavaDoc-1.3-blueviolet.svg) ](https://jiaqijack.bitbucket.io/Circe/site/apidocs/index.html) [ ![License Badge](https://img.shields.io/badge/License-Apache%202.0-orange.svg) ](https://www.apache.org/licenses/LICENSE-2.0)

# Circe

Circe(named on the Greek goddess of transformation) is a collection of Apache Pig UDF's.

Apache Pig provides extensive support for user defined functions (UDFs) as a way to specify custom processing. The UDFs
can be implemented in multiple languages, among which Java is supported most extensively. All parts of the processing
including data load/store, column transformation, and aggregation can be customized. Java functions are also more
efficient because they are implemented in the same language as Apache Pig and because additional interfaces are
supported such as the Algebraic Interface and the Accumulator Interface.

Circe offers assorted UDFs:

* [HBase Storage Optimizer](https://jiaqijack.bitbucket.io/Circe/site/apidocs/org/bitbucket/jiaqijack/circe/AvroPacker.html)
* [HBase row key hashing](https://jiaqijack.bitbucket.io/Circe/site/apidocs/org/bitbucket/jiaqijack/circe/Md5Hash.html)

## Binaries (How to Get It)

Binaries for Circe are stored in [Bintray](https://bintray.com/jiaqijack/Maven/Circe). Dependency information for Maven,
Ivy, and Gradle can be found at https://bintray.com/jiaqijack/Maven/Circe, and some examples are below.

### Maven Example

#### Step 1 - POM

```xml
<dependency>
    <groupId>org.bitbucket.jiaqijack</groupId>
    <artifactId>Circe</artifactId>
    <version>x.y</version>
</dependency>
```

#### Step 2 - settings.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<settings xsi:schemaLocation='http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd'
          xmlns='http://maven.apache.org/SETTINGS/1.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
    
    <profiles>
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-jiaqijack-Maven</id>
                    <name>bintray</name>
                    <url>https://dl.bintray.com/jiaqijack/Maven</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-jiaqijack-Maven</id>
                    <name>bintray-plugins</name>
                    <url>https://dl.bintray.com/jiaqijack/Maven</url>
                </pluginRepository>
            </pluginRepositories>
            <id>bintray</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>bintray</activeProfile>
    </activeProfiles>
</settings>
```

The most recent stable version is: [ ![Stable](https://api.bintray.com/packages/jiaqijack/Maven/Circe/images/download.svg) ](https://bintray.com/jiaqijack/Maven/Circe/_latestVersion)

## License
The use and distribution terms for this software are covered by the Apache License, Version 2.0 ( http://www.apache.org/licenses/LICENSE-2.0.html ).
